#imagen inicial
FROM node

#difinir directorio del contenedor
WORKDIR / colapi_rb

#se añade proyecto al directorio
ADD . /colapi_rb

#puerto escucha
EXPOSE 3000

#Comandos para lanzar
CMD ["npm", "start"]
