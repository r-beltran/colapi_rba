//https://api.mlab.com/api/1/databases?apiKey=whbXeC6Tm0UtntCnUEN4D5eBnQVZ0oYF
//https://api.mlab.com/api/1/databases/colapidb_rba/collections?apiKey=whbXeC6Tm0UtntCnUEN4D5eBnQVZ0oYF
//https://api.mlab.com/api/1/databases/colapidb_rba/collections/account?apiKey=whbXeC6Tm0UtntCnUEN4D5eBnQVZ0oYF
//https://api.mlab.com/api/1/databases/colapidb_rba/collections/account?f={%22_id%22:0}&apiKey=whb
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v2/";

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   usersFile.push(newUser);
   console.log(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

//writeUserDataToFile(userFile)
//us.logged = true;
 // PUT
 app.put(URLbase + 'users/:id',
   function(req, res){
     console.log("PUT /colapi/v2/users/:id");
     var idBuscar = req.params.id;
     var updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       //console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
         res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
   });


   //DELETE
  app.delete(URLbase + 'users/:id',
     function (req, res) {
       console.log("DELETE /colapi/v1/user");
       var encontro = false;
       var idBuscar = req.params.id;
       for(i = 0; i < usersFile.length; i++) {
         if(usersFile[i].id == idBuscar) {
           var encontro=true;
           usersFile.splice(usersFile[i].id,1);
         }
       }
       if (encontro == true){
         console.log("Id Encontrado");
         res.send({"msg" : "Usuario eliminado correctamente."});
       }else{
         console.log("Id No Encontrado");
         res.send({"msg" : "Usuario No Encontrado"});
       }
       res.send('Got a DELETE request at /user');
   });



/*//console.log(req.params);
//console.log(req.headers);
//res.sendfile('./users.json');//deprecated
//res.sendFile(`users.json`,{root: _dirname});
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var usersFile =require('./users.json');
var URLbase ="/colapi/v1/";
var bodyParser =require('body-parser');

app.listen (port);
console.log("Hola soy COLAPI, puerto: "+ port);

app.use(bodyParser.json());

/*app.get(URLbase + 'users/:id',
  function (req, res) {
    console.log("GET/colapi/v1/users/:id");
    var pos = req.params.id;
    res.send(usersFile[pos - 1]);
});

//peticion GET con query
app.get(URLbase + 'users',
  function (peticion, respuesta) {
    console.log("GET con Query");
    console.log(peticion.query);
    respuesta.send({"msg":"GET con Query"});
});

//peticion POST
app.post(URLbase + "users",
  function (req, res) {
    //res.sendFile("users.json",{root: __dirname});
    console.log("POST/colapi/v1");
    console.log(req.body);
    var jsonId = {};
    var newId = usersFile.length +1;
    jsonId = req.body;
    jsonId.id = newId;
    usersFile.push(req.body);
    res.send({"msg":"usr creado correctamente", "id": jsonId});
});

app.put(URLbase + 'users/:id',
  function (req, res) {
    console.log("peticion PUT/colapi/v1/user");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    console.log(req.body);
    for (var user in usersFile){
      if(user == idBuscar){
        console.log("encontradoooooooooooooooooooooooooooooooo");
      }else {
//        console.log(user.id + " - " + idBuscar + " - " + user);
        //console.log("no Encontrado");
      }
    }
    res.send('Got a PUT request at /user');
});

app.delete(URLbase + 'user',
  function (req, res) {
    console.log("DELETE/colapi/v1/user");
    res.send('Got a DELETE request at /user');
});
*/
