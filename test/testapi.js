var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');

var should = chai.should();

chai.use(chaiHttp)//configurar chai con modulo HTTP

describe('Pruebas colombia', () => {
  // it('BBVA funciona ', (done) => {
  //   chai.request('http://www.bbva.com')
  //     .get('/')
  //     .end((err,res) => {
  //       console.log("respuesta "+res.status);
  //       res.should.have.status(200);
  //       done();
  //     })
  // });

  // it('Colapi funciona ', (done) => {
  //   chai.request('http://localhost:3000')
  //     .get('/colapi/v3/users')
  //     .end((err,res) => {
  //       console.log("respuesta "+res.status);
  //       res.should.have.status(200);
  //       done();
  //     })
  // });

  it('Colapi array de usr ', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err,res) => {
        //console.log("respuesta "+res.body);
        //res.body.should.be.a("array");
        res.body.length.should.be.gte(1);
        done();
      })
  });

});
