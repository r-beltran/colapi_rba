var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";
var baseMlabURL = "https://api.mlab.com/api/1/databases/colapidb_rba/collections/";
var apikeyMlab = 'apiKey=whbXeC6Tm0UtntCnUEN4D5eBnQVZ0oYF';

app.listen(port, function(){
 console.log("API apicol v2 escuchando en el puertooooo " + port + "...");
});

app.use(bodyParser.json());

//OK peticion GET todos usuarios
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/user");
   var httpclient = requestJSON.createClient(baseMlabURL);
   console.log("Cliente HTTP mLab creado");
   httpclient.get('user?f={"_id":0}&' + apikeyMlab,
      function(err,respuestaMlab,body){
        console.log("error "+err);
        var respuesta = {};
        respuesta = !err ? body : {"msg":"error en la carga de users en mlab"}
        res.send(respuesta);
      });
});


//OK peticion GET usuario con id
app.get(URLbase + 'users/:id',
 function(req, res) {
   console.log("GET /colapi/v3/user/:id");
   console.log(req.params.id);
   var idBbva = req.params.id;
   var queryString = 'f={"_id":0}&q={"idBbva":"' +idBbva+'"}&';
   var httpclient = requestJSON.createClient(baseMlabURL);
   httpclient.get('user?' + queryString + apikeyMlab,
      function(err,respuestaMlab,body){
        var respuesta = body[0];//sin [0] el array
        res.send(respuesta);
      });
});

//OK peticion GET cuentas de un usr
app.get(URLbase + 'users/:id/account',
 function(req, res) {
   console.log("GET /colapi/v3/users/:id/account");
   var httpclient = requestJSON.createClient(baseMlabURL);
   console.log(req.params.id);
   var idBbva = req.params.id;
   var queryString = 'f={"_id":0}&q={"idBbva":"' +idBbva+'"}&';
   httpclient.get('account?' + queryString + apikeyMlab,
      function(err,respuestaMlab,body){
        var respuesta = {};
        respuesta = !err ? body : {"msg":"error en la carga de account en mlab"}
        console.log(respuesta);
        res.send(respuesta);
        //res.send({"msg":"Respuesta GET ok."});
      });
});

//OK peticion GET todos movement
app.get(URLbase + 'movement',
 function(req, res) {
   console.log("GET /colapi/v3/movement");
   var httpclient = requestJSON.createClient(baseMlabURL);
   httpclient.get('movement?f={"_id":0}&' + apikeyMlab,
      function(err,respuestaMlab,body){
        //console.log("error "+err);
        //console.log("body "+body);
        var respuesta = {};
        respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
        res.send(respuesta);
        //res.send({"msg":"Respuesta GET ok."});
      });
});

//OK peticion GET movements de un usr y cuenta
app.get(URLbase + 'users/:id/:account/movement',
 function(req, res) {
   console.log("GET /colapi/v3/users/:id/:account/movement");
   var httpclient = requestJSON.createClient(baseMlabURL);
   console.log(req.params.id);
   var account = req.params.account;
   var queryString = 'f={"_id":0}&q={"IBAN":"' +account+'"}&';
   httpclient.get('movement?' + queryString + apikeyMlab,
      function(err,respuestaMlab,body){
        var respuesta = {};
        respuesta = !err ? body : {"msg":"error en la carga de account en mlab"}
        console.log(respuesta);
        res.send(respuesta);
        //res.send({"msg":"Respuesta GET ok."});
      });
});

//OK login con mlab
app.post(URLbase+'login',
  function(req, res) {
    console.log(req.body.email);
    console.log(req.body.password);
    var email = req.body.email;
    var queryString = 'f={"_id":0}&q={"email":"' +email+'"}&';
    var respuesta = {};
    var idBbva = "";
    clienteMlab = requestJSON.createClient(baseMlabURL)
    clienteMlab.get('user?' + queryString+apikeyMlab,
        function(err,respuestaMlab,body){
          respuesta = !err ? body[0] : {"msg":"error en la carga de users en mlab"}
          if(respuesta != undefined){
            idBbva = respuesta.idBbva;
            var cambio = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"email":"'+email +'"}&'+apikeyMlab,
              JSON.parse(cambio),
               function(err, respuestaMlab, body) {
                 console.log("Bienvenido");
                 console.log(respuesta);
                 res.send(respuesta);
              }
            )
            //res.send(idBbva);
          }else {
            res.send({"msg":"error en la carga de users en mlab"});
          }
        });
});

//OK logout con mlab
app.post(URLbase+'logout',
  function(req, res) {
   var idBbva=req.body.idBbva;
   console.log(idBbva);
   //GET CONSULTAR PASS Y EMAIL
   console.log("logout");
   var httpClient=requestJSON.createClient(baseMlabURL);
           //PUT agregar login
           var cambio = '{"$unset":{"logged":""}}';
           httpClient.put('user?q={"idBbva":"' + idBbva + '"}&'+ apikeyMlab,
             JSON.parse(cambio),
              function(err, respuestaMlab, body) {
                  console.log(body);
                  res.send({"msg":"sesión cerrada"});
           });
});

//alta usuario
app.post(URLbase+'signup',
  function(req, res) {
    console.log(JSON.stringify(req.body));
    console.log(req.body);
    clienteMlab = requestJSON.createClient(baseMlabURL + "/user?" + apikeyMlab)
    clienteMlab.post('', req.body,
        function(err, resM, body) {
          console.log(body);
           res.send(body)
         })
});



//pruebas ----------------------------------------------------------------------

app.post(URLbase+'users',
  function(req, res) {
    clienteMlab = requestJSON.createClient(baseMlabURL + "/user?" + apikeyMlab)
    clienteMlab.post('', req.body,
        function(err, resM, body) {
           res.send(body)
         })
});

app.put(URLbase+'users/:id',
  function(req, res) {
    console.log(req.params.id);
    clienteMlab = requestJSON.createClient(baseMlabURL + "/user")
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
    clienteMlab.put('?q={"idBbva":"' + req.params.id + '"}&' + apikeyMlab,
    JSON.parse(cambio),
    function(err, resM, body) {
      res.send(body);
    })
})

app.post(URLbase + 'logiiiin',
 function(req, res) {
   console.log(req.body.email);
   console.log(req.body.password);
   var email = req.body.email;
   var password = req.body.password;
   for(user of usersFile){
     if(user.email == email){
       if(user.password == password){
         user.logged = true;
         console.log("correcto");
         writeUserDataToFile(usersFile);
         res.send({"msg" : "Usuario logueado correctamente: ","IdUser": user.id});
      }else{
        res.send({"msg" : "Contraseña no Corresponde"});
      }
     }
   }
 });

 app.post(URLbase + 'logout',
  function(req, res) {
    console.log(req.body.email);
    var email = req.body.email;
    var password = req.body.password;
    for(user of usersFile){
      if(user.email == email && user.logged ==true){
          delete user.logged;
          writeUserDataToFile(usersFile);
          res.send({"msg" : "Sesion cerrada correctamente: ","IdUser": user.id});
      }else{
        res.send({"msg" : "Usuario no Encontrado"});
      }
    }
  });

 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   })
  }
